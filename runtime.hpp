#ifndef RUNTIME_HPP
#define RUNTIME_HPP

#include <vector>
#include <cmds/finde.hpp>
#include <cmds/erstelle.hpp>
#include <cmds/liste.hpp>
#include <cmds/info.hpp>
#include "command.hpp"

#include "cmds/generiere.hpp"
#include "cmds/hilfe.hpp"
#include "json.hpp"
#include "fach.hpp"

#define COUT_NORMAL (char) 27 << "[00m"
#define COUT_YELLOW (char) 27 << "[33m"
#define COUT_RED (char) 27 << "[31m"
#define COUT_GREEN (char) 27 << "[32m"
#define COUT_BLUE (char) 27 << "[34m"
#define COUT_CYAN (char) 27 << "[36m"

using json = nlohmann::json;

class Runtime
{
public:
    static inline std::vector<Command*> commands;

    static std::string read_config(std::string key);
    static void write_config(std::string key, std::string value);
    static void reload_files(bool output=false);
    static void init();
    static bool existsCommand(const std::string &name);
    static Command* getCommand(const std::string &name);
    static void check_fach_directory(Fach f);
    static std::vector<Code> get_codes_for_fach(Fach f, bool output=false);
};


#endif