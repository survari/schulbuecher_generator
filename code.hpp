//
// Created by survari on 12.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_CODE_HPP
#define SCHULBUECHER_GENERATOR_CODE_HPP


#include <string>

class Code
{
public:
    std::string value;
    int line;
    std::string file_name;
};


#endif //SCHULBUECHER_GENERATOR_CODE_HPP
