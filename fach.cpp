//
// Created by survari on 11.08.19.
//

#include "fach.hpp"

std::string Fach::getName() const
{
    return this->name;
}

std::string Fach::getShort() const
{
    return this->_short;
}

int Fach::getID() const
{
    return this->id;
}

void Fach::setID(int id)
{
    this->id = id;
}

void Fach::setName(const std::string &name)
{
    this->name = name;
}

void Fach::setShort(const std::string &_short)
{
    this->_short = _short;
}

Fach::Fach(int id, std::string name, std::string _shorty)
{
    this->id = id;
    this->name = name;
    this->_short = _shorty;
}

std::string Fach::toJSON()
{
    return "[ \""+std::to_string(this->id)+"\", \""+this->name+"\", \""+this->_short+"\" ]";
}

std::string Fach::makePath()
{
    return std::to_string(this->getID())+"_"+this->_short;
}

std::string Fach::makePathWithDelim()
{
    std::string delim = "/";
#if defined(_WIN32) || defined(_WIN64)
    delim = "\\";
#endif

    return std::to_string(this->getID())+"_"+this->_short+delim;
}

Fach::Fach()
{

}
