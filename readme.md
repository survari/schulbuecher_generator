# Schulbüchergenerator

### Einleitung

`Syntax` = Grammatik von Befehlen.

---

#### Erklärung von Wörtern in `<>`

Alles was in `<...>` steht, sind Platzhalter. Sie sind notwendeig für den Befehl.
**Beispiel:**

1. `sag <Text>` → `sag "Hallo Welt!"`
2. `Ich bin <Text>.` → `Ich bin hier.`

---
#### Erklärung von Wörtern in `[]`

`[...]` markieren, wenn ein Teil eines Befehls nicht unbedingt angegeben werden muss. Dieser Teil kann meistens
weggelassen werden.

**Beispiel am Befehl `hilfe`:**
Syntax ist `hilfe [befehlsname]`. Daher können sie entweder nur `hilfe` oder auch einen Befehlsnamen dazu angeben:
`hilfe generiere` (Dies würde einen Hilfetext zu dem Befehl `generiere` anzeigen).

---
#### Erklärung von Wörtern `|` zwischen Wörtern

Ein `|` stellt ein "oder" dar. So heißt `datei|ordner`, dass sie entweder `datei` oder `ordner` an die entsprechende
Stelle schreiben sollten.

---

### Hinweise zur Benutzung

1. Umlaute müssen als `ae` (ä), `oe` (ö), `ue` (ü) und `ss` (ß) angegeben werden.
2. Das Programm sollte in einem extra Ordner liegen, in dem es auch bleiben sollte.
3. Alle Dateien in dem Ordner sollten nicht verändert werden, außer es handelt sich um die generierten CSV-Dateien mit
den Codes. Alle anderen Dateien dienen zur Konfiguration des Programmes und sollten daher nicht verändert werden.
4. Schließen Sie das Programm mit dem Befehl `ende`. Dies speichert alle Änderungen.

---

### Befehle

| Befehl | Syntax | Beschreibung |
| ------ | ------ | ------------ |
| `hilfe`       | `hilfe [befehlsname]`                                    | Zeigt eine Hilfeseite für alle Befehle, oder einen Hilfetext für einen Befehl an. |
| `erstelle`    | `erstelle neues fach <name> <kuerzel> <nummer>`          | Erstellt ein neues Fach mit dem Namen `<name>` und der Identifikationsnummer `<nummer>`. Dazu wird ein neuer Ordner erstellt, in dem alle später generierten Codes auffindbar sind. |
| `generiere`   | `generiere <anzahl> codes fuer fach <nummer>`            | Generiert eine Datei mit `<anzahl>` neuen Codes für das Fach mit der Identifikationsnummer `<nummer>`. Falls noch andere Codes in dem Ordner für das jeweilige Fach zu finden sind, werden diese durchgelesen und berücksichtigt, sodass keine Dopplungen entstehen. |
| `liste`       |  `liste dateien fuer fach <nummer>`                      | Listet alle Dateien für das Fach mit der Identifikationsnummer `<nummer>`. |
|               | `liste alle faecher`                                     | Listet alle Fächer und deren Identifikationsnummern auf.
|               | `liste codes fuer fach <nummer>`                         | Listet alle Codes für das Fach mit der Identifikationsnummer `<nummer>` auf. |
| `info`        | `info zu fach <nummer>`                                  | Listet alle möglichen Informationen über das Fach mit der Identifikationsnummer `<nummer>` auf. |
| `finde`       | `finde code <code>`                                      | Sucht in allen Fächern nach einer Übereinstimmung und gibt das Ergebnis aus. |
|               | `finde duplikate`                                        | Findet doppelte Codes und listet diese auf. Diese Funktion durchsucht Fächer- und Dateienübergreifend alle Codes.
| `neuladen`    | `neuladen`                                               | Lädt die Codes neu ein. Ist Hilfreich, wenn Änderungen an den Dateien vorgenommen wurden. |
| `ende`        | `ende`                                                    | Schließt das Programm sicher. Alle Änderungen werden gespeichert.