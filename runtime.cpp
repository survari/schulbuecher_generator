//
// Created by survari on 11.08.19.
//

#include <fstream>
#include <yaml-cpp/yaml.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <cmds/neuladen.hpp>
#include <cmds/ende.hpp>

#include "runtime.hpp"
#include "json.hpp"
#include "tstring.hpp"

#define COUT_NORMAL (char) 27 << "[00m"
#define COUT_YELLOW (char) 27 << "[33m"
#define COUT_RED (char) 27 << "[31m"
#define COUT_GREEN (char) 27 << "[32m"
#define COUT_BLUE (char) 27 << "[34m"
#define COUT_CYAN (char) 27 << "[36m"

std::string Runtime::read_config(std::string key)
{
    std::ifstream i("config.json");
    json j;
    i >> j;

    return j[key];
}

void Runtime::reload_files(bool output)
{
    std::ifstream i("config.json");
    json j;
    i >> j;
    std::vector<std::vector<std::string>> _f = j["faecher"];

    if (FaecherManager::faecher.size() > _f.size())
    {
        std::string code = "[ ";

        for (Fach &f : FaecherManager::faecher)
        {
            if (output)
                std::cout << "Neues Fach " << f.getID() << " / \"" << f.getName() << "\" wird gespeichert." << std::endl;

            code += f.toJSON()+",";
            f.numbers = get_codes_for_fach(f, output);
        }

        code = code.substr(0, code.size()-1);
        code += " ]";

        j["faecher"] = json::parse(code);
        std::ofstream of("config.json");
        of << j << std::endl;
    }
    else
    {
        FaecherManager::faecher.clear();

        for (std::vector<std::string> s : _f)
        {
            if (output)
                std::cout << "Fach " << s[0] << " / \"" << s[1] << "\" geladen." << std::endl;

            Fach f = Fach(std::stoi(s[0]), s[1], s[2]);
            f.numbers = get_codes_for_fach(f, output);
            FaecherManager::faecher.push_back(f);
        }
    }
}

void Runtime::init()
{
    if (!std::ifstream("config.json").is_open())
    {
        std::ofstream of("config.json");
        of << "{\"faecher\": []}";
        of.close();
    }

    srand(time(0));
    reload_files(true);

    commands.push_back(new Hilfe());
    commands.push_back(new Generiere());
    commands.push_back(new Erstelle());
    commands.push_back(new Finde());
    commands.push_back(new Liste());
    commands.push_back(new Info());
    commands.push_back(new Neuladen());
    commands.push_back(new Ende());
}

bool Runtime::existsCommand(const std::string &name)
{
    bool found = false;

    for (Command* c : commands)
    {
        if (c->getName() == name)
            return true;
    }

    return found;
}

Command* Runtime::getCommand(const std::string &name)
{
    bool found = false;

    for (Command* c : commands)
    {
        if (c->getName() == name)
            return c;
    }
}

void Runtime::write_config(std::string key, std::string value)
{
    std::ifstream i("config.json");
    json j;
    i >> j;

    j[key] = value;
    std::ofstream of("config.json");
    of << j << std::endl;
}

void Runtime::check_fach_directory(Fach f)
{
    struct stat info;
    bool exists = true;
    bool is_file = false;

    if(stat(f.makePath().c_str(), &info) != 0)
        exists = false;
    else is_file = (info.st_mode & S_IFDIR) == 0;

    if (is_file)
    {
        remove(f.makePath().c_str());
        exists = false;
    }

    if (!exists)
    {
#if defined(_WIN32) || defined(_WIN64)
        mkdir(f.makePath().c_str());
#else
        mkdir(f.makePath().c_str(), 0755);
#endif

        reload_files(false);
    }
}

std::vector<Code> Runtime::get_codes_for_fach(Fach f, bool output)
{
    check_fach_directory(f);
    std::vector<std::string> files;
    std::vector<Code> codes;

    DIR* dirp = opendir(f.makePath().c_str());
    struct dirent * dp;

    if (dirp == NULL)
        return codes;

    while ((dp = readdir(dirp)) != NULL)
    {
        if (dp->d_name[0] == '.')
            continue;

        std::string delim = "/";
#if defined(_WIN32) || defined(_WIN64)
        delim = "\\";
#endif

        std::string p = f.makePath()+delim+dp->d_name;
        struct stat s;


        if (stat(p.c_str(), &s) == 0)
        {
            if (s.st_mode & S_IFREG)
            {
                files.push_back(p);
            }
        }
    }
    closedir(dirp);

    for (std::string f : files)
    {
        if (output)
            std::cout << "Lade Codes aus Datei " << f << " ... ";

        std::ifstream _if(f);
        std::string line = "";
        int linec = 1;

        while(std::getline(_if, line))
        {
            if (line[0] != '#' && (line[0] > 47 && line[0] < 58))
            {
                line = tri::string(line).trim().cxs();

                Code c;
                c.value = line;
                c.line = linec;
                c.file_name = f;

                codes.push_back(c);
            }

            linec += 1;
        }

        if (output)
            std::cout << "ok" << std::endl;
    }

    return codes;
}
