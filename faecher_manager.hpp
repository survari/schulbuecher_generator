//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_FAECHER_MANAGER_HPP
#define SCHULBUECHER_GENERATOR_FAECHER_MANAGER_HPP


#include "fach.hpp"

class FaecherManager
{
public:
    static inline std::vector<Fach> faecher;
    static Fach &getFach(int id);

    static bool existsFach(int id);

    static bool existsFachByName(std::string name);

    static bool existsFachByShorty(std::string shorty);
};


#endif //SCHULBUECHER_GENERATOR_FAECHER_MANAGER_HPP
