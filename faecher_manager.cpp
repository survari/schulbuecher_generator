//
// Created by survari on 11.08.19.
//

#include "faecher_manager.hpp"

Fach &FaecherManager::getFach(int id)
{
    for (int i = 0; i < faecher.size(); i++)
    {
        if (faecher[i].getID() == id)
            return faecher[i];
    }
}

bool FaecherManager::existsFach(int id)
{
    for (Fach f : faecher)
    {
        if (f.getID() == id)
            return true;
    }

    return false;
}

bool FaecherManager::existsFachByName(std::string name)
{
    for (Fach f : faecher)
    {
        if (f.getName() == name)
            return true;
    }

    return false;
}

bool FaecherManager::existsFachByShorty(std::string shorty)
{
    for (Fach f : faecher)
    {
        if (f.getShort() == shorty)
            return true;
    }

    return false;
}
