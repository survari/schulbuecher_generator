//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_COMMAND_HPP
#define SCHULBUECHER_GENERATOR_COMMAND_HPP

#include <iostream>
#include <string>

class Command
{
public:
    virtual std::string getName() const { return "<NO_NAME>"; };
    virtual int runFunction(const std::vector<std::string> &args) const { std::cout << "<NO_FUNCTION>" << std::endl; };
    virtual std::string getHelp() const { return "<NO_HELP>"; };
    virtual std::vector<std::string> getSyntax() const { return { "<NO_SYNTAX>" }; };
};


#endif //SCHULBUECHER_GENERATOR_COMMAND_HPP
