//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_FACH_HPP
#define SCHULBUECHER_GENERATOR_FACH_HPP


#include <string>
#include <vector>
#include "code.hpp"

class Fach
{
private:
    std::string name;
    std::string _short;
    int id;

public:
    std::vector<Code> numbers;
    std::string toJSON();
    std::string makePath();
    std::string makePathWithDelim();

    std::string getName() const;
    std::string getShort() const;
    int getID() const;

    void setID(int id);
    void setName(const std::string &name);
    void setShort(const std::string &_short);

    Fach(int id, std::string name, std::string _shorty);
    Fach();
};


#endif //SCHULBUECHER_GENERATOR_FACH_HPP
