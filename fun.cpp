//
// Created by survari on 11.08.19.
//

#include <random>
#include <unistd.h>
#include "fun.hpp"
#include "runtime.hpp"
#include "random.hpp"

void error(std::string message, std::vector<std::string> syntax)
{
    std::cout << char(27) << "[31merror: " << message << std::endl;

    if (syntax.size() > 0)
    {
        std::cout << std::endl << "Syntax: " << syntax[0] << std::endl;

        if (syntax.size() > 1)
        {
            for (int i = 1; i < syntax.size(); i++)
            {
                std::cout << "  oder: " << syntax[i] << std::endl;
            }
        }
    }
}

void reload_files(bool output)
{
    Runtime::reload_files(output);
}

bool yn_question(std::string q)
{
    std::cout << q << std::endl;
    std::cout << "[j/n] : ";
    std::string s;

    while (s != "j" && s != "N" && s != "n" && s != "N")
    {
        std::getline(std::cin, s);

        if (s != "j" && s != "J" && s != "n" && s != "N")
        {
            std::cout << "Bitte gib \"j\" fuer Ja oder \"n\" fuer Nein!" << std::endl;
            std::cout << "[j/n] : ";
        }
    }

    return (s == "j" || s == "J");
}

std::vector<std::string> create_codes(int id, int count, std::vector<std::string> codes, int start_size)
{
    // generiere 5 codes fuer fach 1
    usleep(1);
    std::string tmp;
    std::random_device rd;
    pcg rand(rd);
    std::uniform_int_distribution<> u(0, 9);

    while (count > 0)
    {
        for (int i = 0; i < 9; i++)
        {
            tmp += std::to_string(u(rand));
        }

        tmp = std::to_string(id) + tmp;

        bool found = false;

        for (const std::string &s : codes)
        {
            if (tri::string(s).trim() == tri::string(tmp).trim())
            {
                found = true;
                break;
            }
        }

        if (found)
            continue;

        codes.push_back(tmp);
        tmp = "";
        count -= 1;
    }

    std::vector<std::string> ret(codes.begin()+start_size, codes.end());
    return ret;
}

std::vector<std::string> code_array_to_string_array(const std::vector<Code> &codes)
{
    std::vector<std::string> ret;

    for (const Code &c : codes)
    {
        ret.push_back(c.value);
    }

    return ret;
}
