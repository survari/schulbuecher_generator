# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.15

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/survari/Desktop/Projekte/C++/schulbuecher_generator

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/survari/Desktop/Projekte/C++/schulbuecher_generator

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/ccmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/survari/Desktop/Projekte/C++/schulbuecher_generator/CMakeFiles /home/survari/Desktop/Projekte/C++/schulbuecher_generator/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/survari/Desktop/Projekte/C++/schulbuecher_generator/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named schulbuecher_generator

# Build rule for target.
schulbuecher_generator: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 schulbuecher_generator
.PHONY : schulbuecher_generator

# fast build rule for target.
schulbuecher_generator/fast:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/build
.PHONY : schulbuecher_generator/fast

code.o: code.cpp.o

.PHONY : code.o

# target to build an object file
code.cpp.o:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/code.cpp.o
.PHONY : code.cpp.o

code.i: code.cpp.i

.PHONY : code.i

# target to preprocess a source file
code.cpp.i:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/code.cpp.i
.PHONY : code.cpp.i

code.s: code.cpp.s

.PHONY : code.s

# target to generate assembly for a file
code.cpp.s:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/code.cpp.s
.PHONY : code.cpp.s

fach.o: fach.cpp.o

.PHONY : fach.o

# target to build an object file
fach.cpp.o:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/fach.cpp.o
.PHONY : fach.cpp.o

fach.i: fach.cpp.i

.PHONY : fach.i

# target to preprocess a source file
fach.cpp.i:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/fach.cpp.i
.PHONY : fach.cpp.i

fach.s: fach.cpp.s

.PHONY : fach.s

# target to generate assembly for a file
fach.cpp.s:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/fach.cpp.s
.PHONY : fach.cpp.s

faecher_manager.o: faecher_manager.cpp.o

.PHONY : faecher_manager.o

# target to build an object file
faecher_manager.cpp.o:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/faecher_manager.cpp.o
.PHONY : faecher_manager.cpp.o

faecher_manager.i: faecher_manager.cpp.i

.PHONY : faecher_manager.i

# target to preprocess a source file
faecher_manager.cpp.i:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/faecher_manager.cpp.i
.PHONY : faecher_manager.cpp.i

faecher_manager.s: faecher_manager.cpp.s

.PHONY : faecher_manager.s

# target to generate assembly for a file
faecher_manager.cpp.s:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/faecher_manager.cpp.s
.PHONY : faecher_manager.cpp.s

fun.o: fun.cpp.o

.PHONY : fun.o

# target to build an object file
fun.cpp.o:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/fun.cpp.o
.PHONY : fun.cpp.o

fun.i: fun.cpp.i

.PHONY : fun.i

# target to preprocess a source file
fun.cpp.i:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/fun.cpp.i
.PHONY : fun.cpp.i

fun.s: fun.cpp.s

.PHONY : fun.s

# target to generate assembly for a file
fun.cpp.s:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/fun.cpp.s
.PHONY : fun.cpp.s

main.o: main.cpp.o

.PHONY : main.o

# target to build an object file
main.cpp.o:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/main.cpp.o
.PHONY : main.cpp.o

main.i: main.cpp.i

.PHONY : main.i

# target to preprocess a source file
main.cpp.i:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/main.cpp.i
.PHONY : main.cpp.i

main.s: main.cpp.s

.PHONY : main.s

# target to generate assembly for a file
main.cpp.s:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/main.cpp.s
.PHONY : main.cpp.s

runtime.o: runtime.cpp.o

.PHONY : runtime.o

# target to build an object file
runtime.cpp.o:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/runtime.cpp.o
.PHONY : runtime.cpp.o

runtime.i: runtime.cpp.i

.PHONY : runtime.i

# target to preprocess a source file
runtime.cpp.i:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/runtime.cpp.i
.PHONY : runtime.cpp.i

runtime.s: runtime.cpp.s

.PHONY : runtime.s

# target to generate assembly for a file
runtime.cpp.s:
	$(MAKE) -f CMakeFiles/schulbuecher_generator.dir/build.make CMakeFiles/schulbuecher_generator.dir/runtime.cpp.s
.PHONY : runtime.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... rebuild_cache"
	@echo "... schulbuecher_generator"
	@echo "... edit_cache"
	@echo "... code.o"
	@echo "... code.i"
	@echo "... code.s"
	@echo "... fach.o"
	@echo "... fach.i"
	@echo "... fach.s"
	@echo "... faecher_manager.o"
	@echo "... faecher_manager.i"
	@echo "... faecher_manager.s"
	@echo "... fun.o"
	@echo "... fun.i"
	@echo "... fun.s"
	@echo "... main.o"
	@echo "... main.i"
	@echo "... main.s"
	@echo "... runtime.o"
	@echo "... runtime.i"
	@echo "... runtime.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

