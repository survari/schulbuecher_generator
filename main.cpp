#include <iostream>
#include <vector>

#include <command.hpp>
#include "tstring.hpp"
#include "runtime.hpp"
#include "fun.hpp"

int main()
{
    Runtime::init();
    std::string command;

    while(true)
    {
        printf("\n%c[1;33m> ", 27);
        std::getline(std::cin, command);
        printf("%c[00m", 27);

        std::string tmp;
        std::vector<std::string> args;

        if (tri::string(command).trim().cxs().empty())
        {
            continue;
        }

        bool string = false;

        for (int i = 0; i < command.size(); i++)
        {
            if (command[i] == ' ' && !string)
            {
                if (tmp.size() > 0)
                {
                    args.push_back(tmp);
                    tmp = "";
                }

                continue;
            }
            else if (command[i] == '\"')
            {
                if ((i-1) >= 0)
                {
                    if (command[i-1] != '\\')
                    {
                        string = !string;
                        continue;
                    }
                }
            }

            tmp += command[i];
        }

        if (tmp.size() > 0)
        {
            args.push_back(tmp);
            tmp = "";
        }

        bool found = false;

        for (const Command* c : Runtime::commands)
        {
            if (args[0] == "hilfe")
            {
                found = true;

                if (args.size() > 1)
                {
                    std::cout << (new Hilfe())->generateHelpForCommand(Runtime::getCommand(args[1])) << std::endl;
                    break;
                }

                std::cout << "=== Hilfe ===" << std::endl;
                std::cout << (new Hilfe())->generateHelp(Runtime::commands) << std::endl;
                break;
            }
            else
            {
                if (c->getName() == args[0])
                {
                    found = true;

                    if (c->runFunction(args) == 10)
                    {
                        std::cout << std::endl << "Lade Dateien neu ... ";
                        Runtime::reload_files(false);
                        std::cout << "ok." << std::endl;
                    }

                    break;
                }
            }
        }

        if (!found)
            error("Befehl \""+args[0]+"\" nicht gefunden.", {});
    }
}