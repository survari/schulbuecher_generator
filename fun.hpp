//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_FUN_HPP
#define SCHULBUECHER_GENERATOR_FUN_HPP

#include <iostream>
#include <vector>
#include "code.hpp"

void error(std::string message, std::vector<std::string> syntax);
void reload_files(bool output=false);
bool yn_question(std::string q);
std::vector<std::string> create_codes(int id, int num, std::vector<std::string> codes, int start_size);
std::vector<std::string> code_array_to_string_array(const std::vector<Code> &codes);

#endif //SCHULBUECHER_GENERATOR_FUN_HPP
