//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_FINDE_HPP
#define SCHULBUECHER_GENERATOR_FINDE_HPP

#include <command.hpp>
#include <string>
#include <vector>
#include <fach.hpp>
#include <faecher_manager.hpp>
#include <tstring.hpp>
#include "fun.hpp"

class Finde : public Command
{
public:
    std::string getName() const
    {
        return "finde";
    };

    int runFunction(const std::vector<std::string> &args) const
    {
        if (args.size() != 3 && args.size() != 2)
        {
            error("Zu wenige/viele Argumente!", getSyntax());
            return -1;
        }

        if (args[1] == "duplikate")
        {
            if (args.size() != 2)
            {
                error("Zu wenige/viele Argumente!", getSyntax());
                return -1;
            }

            std::vector<Code> codes;
            std::vector<Code> duplicates;

            for (const Fach &f : FaecherManager::faecher)
            {
                for (const Code &code : f.numbers)
                {
                    for (const Code &check : codes)
                    {
                        if (code.value == check.value)
                        {
                            bool found = false;
                            bool ccfound = false;

                            for (const Code x : duplicates)
                            {
                                if (x.file_name == check.file_name && x.line == check.line && check.value == x.value)
                                {
                                    found = true;
                                    break;
                                }
                            }

                            for (const Code x : duplicates)
                            {
                                if (x.file_name == code.file_name && x.line == code.line && code.value == x.value)
                                {
                                    ccfound = true;
                                    break;
                                }
                            }

                            if (!found)
                                duplicates.push_back(check);

                            if (!ccfound)
                                duplicates.push_back(code);
                        }
                    }

                    codes.push_back(code);
                }
            }

            if (duplicates.size() == 0)
                std::cout << "Keine Duplikate gefunden." << std::endl;

            std::vector<Code> finished_duplicates;

            for (const Code &c : duplicates)
            {
                bool found = false;

                for (const Code &x : finished_duplicates)
                {
                    if (x.value == c.value)
                        found = true;
                }

                if (found)
                    continue;

                std::cout << std::endl;
                std::cout << "Duplikate zum Code \"" << c.value << "\" in folgenden Dateien:" << std::endl;
                finished_duplicates.push_back(c);

                for (const Code &x : duplicates)
                {
                    if (x.value == c.value)
                        std::cout << "    - " << x.file_name << ", Zeile " << x.line << std::endl;
                }
            }

            return 0;
        }

        if (args[1] != "code")
        {
            error("Sie haben einen Fehler im Syntax!", getSyntax());
            return -1;
        }

        int count = 0;

        for (const Fach &f : FaecherManager::faecher)
        {
            for (const Code &s : f.numbers)
            {
                if (tri::string(s.value).trim().cxs() == tri::string(args[2]).trim().cxs())
                {
                    std::cout << "Fund in Fach " << "(" << f.getID() << ") " << f.getName() << " / " << f.getShort() << ",\nin Datei " << s.file_name << " Zeile " << s.line << std::endl << std::endl;
                    count += 1;
                }
                else if (tri::string(s.value).trim().cxs().find(tri::string(args[2]).trim().cxs()) != std::string::npos)
                {
                    std::cout << "Nicht komplette Uebereinstimmung im Fach (" << f.getID() << ") " << f.getName() << " / " << f.getShort() << ",\nin Datei " <<  s.file_name << " in Zeile " << s.line << std::endl << "    - " << s.value << " enthaelt " << args[2] << std::endl << std::endl;
                }
            }
        }

        std::string e = "e";

        if (count == 1)
            e = "";

        if (count > 0)
            std::cout << std::endl;

        std::cout << "Insgesamt " << count << " Fund" << e << "." << std::endl;
    };

    std::string getHelp() const
    {
        return "Sucht in allen Faechern nach einer Uebereinstimmung und gibt das Ergebnis aus.";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "finde code <code>" };
    };
};

#endif //SCHULBUECHER_GENERATOR_FINDE_HPP
