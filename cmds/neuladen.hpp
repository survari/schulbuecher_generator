//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_NEULADEN_HPP
#define SCHULBUECHER_GENERATOR_NEULADEN_HPP

#include <command.hpp>
#include <string>
#include <vector>
#include <fun.hpp>
#include <fach.hpp>
#include <runtime.hpp>
#include <faecher_manager.hpp>

class Neuladen : public Command
{
public:
    std::string getName() const
    {
        return "neuladen";
    };

    int runFunction(const std::vector<std::string> &args) const
    {
        reload_files(true);
    };

    std::string getHelp() const
    {
        return "Laedt die Codes neu ein. Ist Hilfreich, wenn Aenderungen an den Dateien vorgenommen wurden.";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "neuladen" };
    };
};

#endif //SCHULBUECHER_GENERATOR_NEULADEN_HPP
