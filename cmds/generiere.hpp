//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_GENERIERE_HPP
#define SCHULBUECHER_GENERATOR_GENERIERE_HPP

#include <command.hpp>
#include <vector>
#include <iostream>
#include <ctime>
#include <fstream>

class Generiere : public Command
{
public:
    std::string getName() const
    {
        return "generiere";
    };

    int runFunction(const std::vector<std::string> &args) const
    {

        if (args.size() != 6)
        {
            error("Sie haben zu wenige/viele Argumente angegeben!", getSyntax());
            return -1;
        }

        if ((args[2] != "code" && args[2] != "codes") || args[3] != "fuer" || args[4] != "fach")
        {
            error("Sie haben einen Fehler im Syntax!", getSyntax());
            return -1;
        }

        int num = 0;
        int count = 0;

        try
        {
            num = std::stoi(args[5]);
            count = std::stoi(args[1]);
        }
        catch(std::exception ex)
        {
            error("Sie müssen eine Nummer angeben.", getSyntax());
            return -1;
        }

        if (!FaecherManager::existsFach(num))
        {
            error("Ein Fach mit der ID \""+std::to_string(num)+"\" existiert nicht. Erstellen Sie es mit dem Befehl \"erstelle\".", getSyntax());
            return -1;
        }

        if (count < 0)
        {
            std::cout << "Soll ich etwa Anti-Codes generieren oder Codes abziehen?! Eine Anzahl im Minusbereich kann ich nicht bearbeiten." << std::endl;
            return -1;
        }

        if (count > 1000)
        {
            error("Es sollten nicht mehr als 1000 Codes auf einmal generiert werden. Das haelt der Computer nicht aus. Es koennte aber auch klappen und einige Sekunden lang dauern (getestet wurden 10000, die haben 30 Sekunden gedauert).", {});

            if (!yn_question("Moechten Sie trotzdem fortfahren?"))
                return -1;
        }

        Fach &f = FaecherManager::getFach(num);
        std::time_t t = std::time(0);
        std::tm* now = std::localtime(&t);
        std::string year = std::to_string(now->tm_year + 1900);
        std::string month = std::to_string(now->tm_mon + 1);
        std::string day = std::to_string(now->tm_mday);
        std::string hour = std::to_string(now->tm_hour);
        std::string minute = std::to_string(now->tm_min);
        std::string second = std::to_string(now->tm_sec);
        std::string file_name = std::to_string(f.getID())+"_"+f.getShort()+"_"+std::to_string(count)+"_"+year+"."+month+"."+day+"_"+hour+"."+minute+"."+second+".csv";

        std::cout << "Generiere ... ";
        std::vector<std::string> codes = create_codes(f.getID(), count, code_array_to_string_array(f.numbers), f.numbers.size());
        std::cout << "ok." << std::endl;

        std::cout << "Erstelle neue Datei " << file_name << " ... ";
        std::ofstream of(f.makePathWithDelim()+file_name);
        of << "f_" << f.getName() << "_" << f.getID() << std::endl;

        for (const std::string code : codes)
            of << code << std::endl;

        std::cout << "ok." << std::endl;
        return 10;
    };

    std::string getHelp() const
    {
        return "Generiert eine Datei mit <anzahl> neuen Codes für das Fach mit der Identifikationsnummer <nummer>. Falls noch andere Codes in dem Ordner für das jeweilige Fach zu finden sind, werden diese durchgelesen und berücksichtigt, sodass keine Dopplungen entstehen.";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "generiere <anzahl> codes fuer fach <nummer>" };
    };
};

#endif //SCHULBUECHER_GENERATOR_GENERIERE_HPP
