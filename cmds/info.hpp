//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_INFO_HPP
#define SCHULBUECHER_GENERATOR_INFO_HPP

#include <command.hpp>
#include <string>
#include <vector>

class Info : public Command
{
public:
    std::string getName() const
    {
        return "info";
    };

    int runFunction(const std::vector<std::string> &args) const
    {
        if (args.size() != 4)
        {
            error("Zu wenige/viele Argumente!", getSyntax());
            return -1;
        }

        if (args[1] != "zu" || args[2] != "fach")
        {
            error("Sie haben einen Fehler im Syntax!", getSyntax());
            return -1;
        }

        Fach f;

        try
        {
            if (!FaecherManager::existsFach(std::stoi(args[3])))
            {
                error("So ein Fach existiert nicht. Erstellen Sie es mit dem Befehl \"erstelle\".", {});
                return 0;
            }

            f = FaecherManager::getFach(std::stoi(args[3]));
        }
        catch (std::exception ex)
        {
            error("Sie müssen eine Nummer eingeben.", {});
            return 0;
        }

        std::cout << "Fach                  : " << f.getName() << std::endl;
        std::cout << "Kuerzel               : " << f.getShort() << std::endl;
        std::cout << "Idetifikationsnummer  : " << f.getID() << std::endl;
        std::cout << std::endl;
        std::string s = "s";

        if (f.numbers.size() == 1)
            s = "";

        std::cout << "Insgesamt " << f.numbers.size() << " Code"+s+"." << std::endl;
    };

    std::string getHelp() const
    {
        return "Listet alle möglichen Informationen über das Fach mit der Identifikationsnummer `<nummer>` auf.";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "info zu fach <nummer>" };
    };
};

#endif //SCHULBUECHER_GENERATOR_INFO_HPP
