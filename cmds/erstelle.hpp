//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_ERSTELLE_HPP
#define SCHULBUECHER_GENERATOR_ERSTELLE_HPP

#include <command.hpp>
#include <string>
#include <vector>
#include <fun.hpp>

class Erstelle : public Command
{
public:
    std::string getName() const
    {
        return "erstelle";
    };

    int runFunction(const std::vector<std::string> &args) const
    {
        if (args.size() != 6)
        {
            error("Sie haben zu wenige/viele Argumente angegeben!", getSyntax());
            return -1;
        }

        if (args[1] != "neues" || args[2] != "fach")
        {
            error("Sie haben einen Fehler im Syntax!", getSyntax());
            return -1;
        }

        int num = 0;
        std::string name = args[3];
        std::string shorty = args[4];

        try
        {
            num = std::stoi(args[5]);
        }
        catch(std::exception ex)
        {
            error("Sie müssen eine Nummer angeben.", getSyntax());
            return -1;
        }

        if (FaecherManager::existsFach(num))
        {
            error("Ein Fach mit dieser ID existiert schon.", getSyntax());
            return -1;
        }

        if (FaecherManager::existsFachByName(name) == true)
        {
            if (!yn_question("Ein Fach mit diesem Namen existiert schon.\nMoechten Sie trotzdem fortfahren?"))
                return -1;
        }

        if (FaecherManager::existsFachByShorty(name) == true)
        {
            if (!yn_question("Ein Fach mit diesem Kuerzel existiert schon.\nMoechten Sie trotzdem fortfahren?"))
                return -1;
        }

        FaecherManager::faecher.push_back(Fach(num, name, shorty));
        return 10;
    };

    std::string getHelp() const
    {
        return "Erstellt ein neues Fach mit dem Namen <name> und der Identifikationsnummer <nummer>. Dazu wird ein neuer Ordner erstellt, in dem alle später generierten Codes auffindbar sind.";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "erstelle neues fach <name> <kuerzel> <nummer>" };
    };
};

#endif //SCHULBUECHER_GENERATOR_ERSTELLE_HPP
