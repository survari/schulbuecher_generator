//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_LISTE_HPP
#define SCHULBUECHER_GENERATOR_LISTE_HPP

#include <command.hpp>
#include <string>
#include <vector>
#include <fun.hpp>
#include <fach.hpp>
#include <runtime.hpp>
#include <faecher_manager.hpp>
#include <dirent.h>
#include <sys/stat.h>

class Liste : public Command
{
public:
    std::string getName() const
    {
        return "liste";
    };

    int runFunction(const std::vector<std::string> &args) const
    {
        if (args.size() != 3 && args.size() != 5)
        {
            error("Sie haben zu wenige/viele Argumente angegeben!", getSyntax());
            return -1;
        }

        if (args[1] == "dateien" && args[2] == "fuer" && args[3] == "fach")
        {
            Fach f;

            try
            {
                if (!FaecherManager::existsFach(std::stoi(args[4])))
                {
                    error("So ein Fach existiert nicht. Erstellen Sie es mit dem Befehl \"erstelle\".", {});
                    return 0;
                }

                f = FaecherManager::getFach(std::stoi(args[4]));
            }
            catch (std::exception ex)
            {
                error("Sie müssen eine Nummer eingeben.", {});
                return 0;
            }

            DIR* dirp = opendir(f.makePath().c_str());
            struct dirent * dp;

            if (dirp == NULL)
            {
                std::cout << "Keine Dateien." << std::endl;
                return 0;
            }

            while ((dp = readdir(dirp)) != NULL)
            {
                if (dp->d_name[0] == '.')
                    continue;

                std::string delim = "/";
#if defined(_WIN32) || defined(_WIN64)
                delim = "\\";
#endif

                std::string p = f.makePath()+delim+dp->d_name;
                struct stat s;


                if (stat(p.c_str(), &s) == 0)
                {
                    if (s.st_mode & S_IFREG)
                    {
                        std::cout << dp->d_name << std::endl;
                    }
                }
            }
        }
        else if (args[1] == "alle" && args[2] == "faecher")
        {
            for (Fach f : FaecherManager::faecher)
            {
                std::cout << "(" << f.getID() << ") " << f.getName() << " / " << f.getShort() << " hat insgesamt " << f.numbers.size() << " Codes." << std::endl;
            }
        }
        else if (args[1] == "codes" && args[2] == "fuer" && args[3] == "fach")
        {
            int nummer = 0;

            try
            {
                nummer = std::stoi(args[4]);
            }
            catch (std::exception e)
            {
                error("Sie müssen eine Zahl angeben.", {});
                return -1;
            }

            Fach f = FaecherManager::getFach(nummer);

            for (const Code &c : f.numbers)
            {
                std::cout << c.value << std::endl;
            }

            std::string s = "s";

            if (f.numbers.size() == 1)
                s = "";

            std::cout << "Insgesamt " << f.numbers.size() << " Code." << s << std::endl;
        }
        else
        {
            error("Sie haben einen Error im Syntax!", getSyntax());
            return -1;
        }
    };

    std::string getHelp() const
    {
        return "erstelle neues fach <name> <kuerzel> <nummer>";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "liste dateien fuer fach <nummer>", "liste alle faecher", "liste codes fuer fach <nummer>" };
    };
};

#endif //SCHULBUECHER_GENERATOR_LISTE_HPP
