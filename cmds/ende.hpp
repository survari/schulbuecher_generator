//
// Created by survari on 12.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_ENDE_HPP
#define SCHULBUECHER_GENERATOR_ENDE_HPP

#include <command.hpp>
#include <vector>
#include <iostream>
#include <ctime>
#include <fstream>
#include <fun.hpp>

class Ende : public Command
{
public:
    std::string getName() const
    {
        return "ende";
    };

    int runFunction(const std::vector<std::string> &args) const
    {
        std::cout << "Speichere Aenderungen..." << std::endl;
        reload_files(true);
        exit(0);
    };

    std::string getHelp() const
    {
        return "Beendet das Programm sicher.";
    };

    std::vector<std::string> getSyntax() const
    {
        return { "ende" };
    };
};

#endif //SCHULBUECHER_GENERATOR_ENDE_HPP
