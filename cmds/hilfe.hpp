//
// Created by survari on 11.08.19.
//

#ifndef SCHULBUECHER_GENERATOR_HILFE_HPP
#define SCHULBUECHER_GENERATOR_HILFE_HPP


#include <string>
#include <command.hpp>
#include <vector>

class Hilfe : public Command
{
public:
    std::string getName() const
    {
        return "hilfe";
    }

    int runFuntion(const std::vector<std::string> &args) const
    {

    }

    std::string generateHelp(const std::vector<Command*> &commands)
    {
        std::string ret;
        std::string spaces;
        int max_len = 0;

        ret += "-> <befehlsname> - <befehlssyntax>\n\n";

        for (const Command* c : commands)
        {
            if (c->getName().size() > max_len)
                max_len = c->getName().size();
        }

        max_len++;

        for (const Command* c : commands)
        {
            spaces = "";
            for (int i = 0; i < (max_len-c->getName().size()); i++)
                spaces += " ";

            ret += "->  "+c->getName()+spaces+"- "+c->getSyntax()[0]+"\n";

            std::string stmp = spaces;
            spaces = "";

            for (int i = 0; i < (std::string("->  "+c->getName()+stmp).size()); i++)
                spaces += " ";

            if (c->getSyntax().size() > 1)
            {
                for (int i = 1; i < c->getSyntax().size(); i++)
                {
                    ret += spaces+"- "+c->getSyntax()[i]+"\n";
                }
            }

            ret += "\n"+c->getHelp()+"\n";
            ret += "\n";
        }

        return ret+"Für weitere Hilfe zu dem Programm siehe die ReadMe.md-Datei.";
    }

    std::string generateHelpForCommand(Command* command)
    {
        std::vector<Command*> v;
        v.push_back(command);

        return Hilfe::generateHelp(v);
    }

    std::string getHelp() const
    {
        return "Zeigt hilfe an.";
    }

    std::vector<std::string> getSyntax() const
    {
        return { "hilfe [Befehl]" };
    }
};


#endif //SCHULBUECHER_GENERATOR_HILFE_HPP
